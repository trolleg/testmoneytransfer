package ru.test.account.api.dto;

public class TransferDTO {
    private String account1;
    private String account2;
    private int amount;

    public TransferDTO(String a1, String s2, int a) {
        account1 = a1;
        account2 = s2;
        amount = a;

    }
    public String getAccount1() {
        return account1;
    }

    public void setAccount1(String account1) {
        this.account1 = account1;
    }

    public String getAccount2() {
        return account2;
    }

    public void setAccount2(String account2) {
        this.account2 = account2;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
