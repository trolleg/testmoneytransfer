package ru.test.account.test;

import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import static org.junit.Assert.assertEquals;

public class CommonTest {

    private static WebTarget target;
    @BeforeClass
    public static void init() {
        Client client = ClientBuilder.newClient();
        target = client.target("http://localhost:8080/");
    }

    @Test
    public void testSimpleCase() {
        assertEquals(0, ManualClientTest.createAccount("11131", 101, target));
        assertEquals(0, ManualClientTest.createAccount("11132", 99, target));
        assertEquals(0, ManualClientTest.createTransfer("11131", "11132", 4, target));
    }

    @Test
    public void testOneCase() {
        assertEquals(0, ManualClientTest.createAccount("11111", 101, target));
        assertEquals(0, ManualClientTest.createAccount("11112", 99, target));
        assertEquals(101, ManualClientTest.checkAccount("11111", target));
        assertEquals(99, ManualClientTest.checkAccount("11112", target));
        assertEquals(0, ManualClientTest.createTransfer("11111", "11112", 4, target));
        assertEquals(97, ManualClientTest.checkAccount("11111", target));
        assertEquals(103, ManualClientTest.checkAccount("11112", target));
    }

    @Test
    public void testCreateAlreadyEistsAccount() {
        assertEquals(0, ManualClientTest.createAccount("11161", 101, target));
        assertEquals(4, ManualClientTest.createAccount("11161", 101, target));
    }

    @Test
    public void testNegativeCreateAccount() {
        assertEquals(5, ManualClientTest.createAccount("1111", 101, target));
        assertEquals(5, ManualClientTest.createAccount("111s1", 101, target));
        assertEquals(6, ManualClientTest.createAccount("11111", -101, target));
    }

    @Test
    public void testNegativeAmountTransfer() {
        assertEquals(0, ManualClientTest.createAccount("11113", 101, target));
        assertEquals(0, ManualClientTest.createAccount("11114", 99, target));
        assertEquals(6, ManualClientTest.createTransfer("11113", "11114", 0, target));
        assertEquals(6, ManualClientTest.createTransfer("11113", "11114", -1, target));
    }

    @Test
    public void testNegativeNoMoneyTransfer() {
        assertEquals(0, ManualClientTest.createAccount("11151", 101, target));
        assertEquals(0, ManualClientTest.createAccount("11152", 99, target));
        assertEquals(3, ManualClientTest.createTransfer("11151", "11152", 102, target));
    }

    @Test
    public void testNegativeAccountDoesntExists() {
        assertEquals(1, ManualClientTest.createTransfer("11120", "11121", 1, target));
    }

}
